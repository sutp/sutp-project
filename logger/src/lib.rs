use colored::*;

pub fn fatal(msg: String) {
    println!(
        "{header}: {body}",
        header = "FATAL".bright_red().bold(),
        body = msg
    );
    std::process::exit(1);
}

pub fn warn(msg: String) {
    println!(
        "{header}: {body}",
        header = "WARNING".bright_yellow().bold(),
        body = msg
    );
}

pub fn info(msg: String) {
    println!(
        "{header}: {body}",
        header = "INFO".bright_cyan().bold(),
        body = msg
    );
}
