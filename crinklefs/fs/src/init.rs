use crate::block::{Drive, InodeModes, SuperBlock, FS_SIZE};
use logger::{fatal, info};
use std::io::Write;
use std::path::Path;

pub fn init(device: Drive) {
    let path = Path::new(&device.path);

    let file = std::fs::OpenOptions::new()
        .create(true)
        .write(true)
        .read(true)
        .truncate(true)
        .open(path.clone());

    if file.is_err() {
        info(format!("internal error: {}", file.as_ref().err().unwrap()));
        fatal(format!(
            "Broken disk image file at {}",
            path.as_os_str().to_str().unwrap()
        ))
    }

    file.unwrap().write_all(&[0; FS_SIZE as usize]).ok();
}

pub fn start(device: Drive) {
    let path = Path::new(&device.path);
    let file = std::fs::OpenOptions::new()
        .write(true)
        .read(true)
        .open(path.clone());
    if file.is_err() {
        fatal("Failed to start fs".to_string())
    }
}
