use std::{
    fs::OpenOptions,
    io::{Read, Seek, SeekFrom, Write},
};

use byteorder::{BigEndian, LittleEndian, ReadBytesExt};
use chrono::Datelike;
use logger::{fatal, info, warn}; // 1.2.7

use crate::block::{Drive, Inode, InodeModes, InodeTimestamp, BLK_SIZE, FS_SIZE};

pub fn find_inode(d: Drive, id: u64) -> Inode {
    let mut f = OpenOptions::new()
        .read(true)
        .write(true)
        .open(d.path.clone())
        .unwrap();
    let index = id * BLK_SIZE;
    if f.seek(SeekFrom::Start(index)).is_err() {
        panic!("broken disk image file");
    }
    let mut node_buffer: [u8; 4000] = [0; 4000];
    if index > FS_SIZE {
        fatal(format!(
            "Failed to index at {} for filesystem of size {}",
            index, FS_SIZE
        ))
    }
    let read_to = f.read_exact(&mut node_buffer);
    if read_to.is_err() {
        info(format!("internal error: {}", read_to.err().unwrap()));
        fatal(format!("Broken disk image file at {}", d.path))
    }

    let mut inode = Inode::new(id);
    inode.raw_buffer = node_buffer;

    return inode;
}

impl Inode {
    pub fn new(id: u64) -> Self {
        let node = Inode {
            id,
            raw_buffer: [0; 4000],
        };

        return node;
    }
    pub fn set_id(&mut self, id: u64) -> &mut Inode {
        self.id = id;
        return self;
    }
    pub fn set_blk_type(&mut self, t: InodeModes) -> &mut Inode {
        self.raw_buffer[0] = t as u8;
        return self;
    }
    pub fn set_create_time(&mut self) -> &mut Inode {
        let current_date = chrono::Utc::now();

        self.raw_buffer[1] = current_date.day() as u8;
        self.raw_buffer[2] = current_date.month() as u8;
        let bytes = current_date.year().to_le_bytes();

        self.raw_buffer[3] = bytes[0];
        self.raw_buffer[4] = bytes[1];
        self.raw_buffer[5] = bytes[2];
        self.raw_buffer[6] = bytes[3];

        return self;
    }
    pub fn set_update_time(&mut self) -> &mut Inode {
        let current_date = chrono::Utc::now();

        self.raw_buffer[7] = current_date.day() as u8;
        self.raw_buffer[8] = current_date.month() as u8;
        let bytes = current_date.year().to_le_bytes();

        self.raw_buffer[9] = bytes[0];
        self.raw_buffer[10] = bytes[1];
        self.raw_buffer[11] = bytes[2];
        self.raw_buffer[12] = bytes[3];

        return self;
    }
    pub fn set_u32(&mut self, index: usize, data: u32) {
        let offset_index: usize = index + 13;

        if self.raw_buffer[offset_index] != 0 {
            warn("writing over written bytes in block".to_string())
        }

        let bytes = data.to_le_bytes();
        self.raw_buffer[offset_index] = bytes[0];
        self.raw_buffer[offset_index + 1] = bytes[1];
        self.raw_buffer[offset_index + 2] = bytes[2];
        self.raw_buffer[offset_index + 3] = bytes[3];
    }

    pub fn set_u16(&mut self, index: usize, data: u32) {
        let offset_index: usize = index + 13;

        if self.raw_buffer[offset_index] != 0 {
            warn("writing over written bytes in block".to_string())
        }

        let bytes = data.to_le_bytes();
        self.raw_buffer[offset_index] = bytes[0];
        self.raw_buffer[offset_index + 1] = bytes[1];
    }

    pub fn get_u32(&mut self, index: usize) -> u32 {
        let offset_index: usize = index + 13;
        let mut data = &self.raw_buffer[offset_index..offset_index + 5];
        let udata = data.read_u32::<LittleEndian>().unwrap();

        return udata;
    }
    pub fn get_u16(&mut self, index: usize) -> u16 {
        let offset_index: usize = index + 13;
        let mut data = &self.raw_buffer[offset_index..offset_index + 3];
        let udata = data.read_u16::<LittleEndian>().unwrap();

        return udata;
    }

    pub fn get_timestamps(&mut self) -> InodeTimestamp {
        let create = &self.raw_buffer[1..7];
        let update = &self.raw_buffer[7..13];

        let mut create_year = &create[2..6];
        let mut update_year = &update[2..6];

        let c_year = create_year.read_u32::<LittleEndian>().unwrap();
        let u_year = update_year.read_u32::<LittleEndian>().unwrap();

        let create_str = format!(
            "{day}/{month}/{year}",
            day = create[0].to_string(),
            month = create[1].to_string(),
            year = c_year.to_string()
        );

        let update_str = format!(
            "{day}/{month}/{year}",
            day = std::str::from_utf8(&[update[0]]).unwrap(),
            month = std::str::from_utf8(&[update[1]]).unwrap(),
            year = u_year.to_string()
        );

        return InodeTimestamp {
            create_time: create_str,
            update_time: update_str,
        };
    }

    pub fn write(&mut self, d: Drive) -> &mut Inode {
        let mut f = OpenOptions::new()
            .read(true)
            .write(true)
            .append(false)
            .open(d.path)
            .unwrap();

        if f.seek(SeekFrom::Start(self.id * 4000)).unwrap() != self.id {
            panic!("broken disk image file");
        }

        if self.raw_buffer[0] == 0 {
            self.set_create_time();
        }

        f.write_all(&self.raw_buffer).unwrap();
        return self;
    }
}
