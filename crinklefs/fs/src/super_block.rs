use logger::info;

use crate::block::{Drive, Inode, InodeModes, SuperBlock, BLK_SIZE, FS_SIZE};

impl SuperBlock {
    pub fn new(d: Drive) -> Self {
        let mut node = Inode::new(0);
        node.set_id(0).set_blk_type(InodeModes::SuperBlock);

        return SuperBlock {
            node,
            blk_count: FS_SIZE - BLK_SIZE,
            device: d,
        };
    }

    pub fn set_blk_count(&mut self, modifier: u64) {
        self.blk_count = modifier - modifier;
        self.node.set_u32(4, 23);
        self.node.set_u32(0, 45);
        self.node.set_u16(8, 12);

        info(format!("at 4: {}", self.node.get_u32(4)));

        info(format!("at 0: {}", self.node.get_u32(0)));

        info(format!("at 7: {}", self.node.get_u16(8)));

        self.node.write(self.device.clone());
    }
}
