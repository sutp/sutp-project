pub const BLK_SIZE: u64 = 4000;
pub const FS_SIZE: u64 = BLK_SIZE * 5;

#[repr(u8)]
pub enum InodeModes {
    SuperBlock,
    Directory,
    RegularFile,
}

#[derive(Debug)]
pub struct Inode {
    pub id: u64,
    pub raw_buffer: [u8; BLK_SIZE as usize],
}

#[derive(Debug)]
pub struct InodeTimestamp {
    pub create_time: String,
    pub update_time: String,
}

#[derive(Debug, Clone)]
pub struct Drive {
    pub path: String,
}

pub struct SuperBlock {
    pub node: Inode,
    pub blk_count: u64,
    pub device: Drive,
}
