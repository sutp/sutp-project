export default function pageTitle({Name}) {
    return (
        <p className="text-2xl mt-4 font-sans"> &gt;&gt;&gt;&gt; {Name}</p>
    )
}
