export default function Menu() {
  return (
      <div id = "menu" className = "grid grid-flow-col msm:pb-10"><div>
      <p id = "title" className = "font-sans">SUTP</p>
        <p id = "sub-title" className = "font-mono text-sm msm:invisible msm:hidden">Someones Unified Technology Platform</p>
      </div>
      <div className="flex flex-row justify-end text-sm">
        <p id = "menu" className = "font-mono mx-5 cursor-pointer" onClick={() => window.location.href = "/"}>HOME</p>
      <p id = "menu" className = "font-mono mx-5 cursor-pointer" onClick={() => window.location.href = "/source"}>SOURCE</p>
        <p id = "menu" className = "font-mono mx-5 cursor-pointer" onClick={() => window.location.href = "/about"}>ABOUT</p>
      </div>
    </div>)
}
