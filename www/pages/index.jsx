import Header from "../components/header.jsx"

export default function Home() {

  return (
      <>
        <Header Name="CONTENT"/>
        <div className="mt-5 text-sm font-sans"><span className="font-mono">MISSION:</span> Define software for the hardware of now and the future.</div>
      </>
  );
}
