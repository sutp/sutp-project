import '../styles/globals.css'
import Menu from '../components/menu.jsx'

function MyApp({ Component, pageProps }) {
  return (
    <div className='msm:m-0 msm:mt-5 m-12'>
      <Menu/>
      <Component {...pageProps} />
    </div>
  )
}

export default MyApp
